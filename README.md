IronAshley is a C# port of the entity component system Ashley (https://github.com/libgdx/ashley). 
As like Ashley, IronAshley tries to be a high-performance entity framework without the use of black-magic and thus making the API easy and transparent to use. 
IronAshley is based on NET framework 2.0 (or Mono). 
Precompiled binary: http://files.infectedbytes.com/ironashley/IronAshley.dll

### Differences between Ashley and IronAshely
Some things are a bit different in IronAshley, because the design philosophy of C# is a bit different from Java. 
#### Naming
First of all the naming is a different. While Java uses camelCase, C# uses PascalCase. 
Also the naming of interfaces is different. In C# the name of an interface will usually start with an I, like IListener, but in Java you just use the name without any special letter in front of it. 
Furthermore Java users will use methods like size() or length() to determine the amount of elements in a collection. C# users will use a get property named Count. 

#### Generics
Another difference is the handling of generics. 
In Java one often uses something like this:
```java
public <T extends Component> T getComponent (Class<T> componentClass) {
	// ...
}
// ...
PositionComponent pos = getComponent(PositionComponent.class);
```
In C# one would prefer this:
```c#
public T GetComponent<T>() where T : IComponent {
	// ...
}
// ...
PositionComponent pos = GetComponent<PositionComponent>();
```
IronAshley uses the later one whenever possible, but a similar approach like the java version is also available. 

#### External classes
Ashley uses some Classes from the LibGdx Framework (like some Collections). 
Some of those classes are ported to C#, but some others are not (like the ImmutableArray class). 
Instead of porting those classes, IronAshley uses classes from the NET-Framework.
Here is a small overview:
```
Ashley         | IronAshley
-------------- | --------------
Array          | List
ImmutableArray | ReadOnlyCollection
LongMap        | Dictionary
ObjectMap      | Dictionary
SnapshotArray  | (combination of arrays and List)
Bag            | ported
Bits           | ported
Pool           | ported
ReflectionPool | ported
```