﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Test {
    public class MovementSystem : EntitySystem {
        private ReadOnlyCollection<Entity> entities;

        private ComponentMapper<PositionComponent> pm = ComponentMapper<PositionComponent>.Get();
        private ComponentMapper<VelocityComponent> vm = ComponentMapper<VelocityComponent>.Get();

        public MovementSystem() { }

        public override void AddedToEngine(Engine engine) {
            entities = engine.GetEntitiesFor(Family.All<PositionComponent, VelocityComponent>());
        }

        public override void Update(float deltaTime) {
            for(int i = 0; i < entities.Count; ++i) {
                Entity entity = entities[i];
                PositionComponent position = pm.Get(entity);
                VelocityComponent velocity = vm.Get(entity);

                position.X += velocity.X * deltaTime;
                position.Y += velocity.Y * deltaTime;
            }
        }
    }
}
