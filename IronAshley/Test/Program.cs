﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Test {
    class Program {
        static Random rnd = new Random();
        static Engine engine;
        static void Main(string[] args) {
            engine = new Engine();
            engine.AddSystem(new MovementSystem());
            engine.AddEntity(Build());
            engine.AddEntity(Build());
            engine.AddEntity(Build());

            for(int i = 0; i < 3; i++) {
                Console.WriteLine("Update {0}", i);
                engine.Update(10);
                PrintEntities();
            }
            Console.WriteLine("Remove Velocity from ID 2");
            engine.GetEntity(2).Remove<VelocityComponent>();
            PrintEntities();

            Console.ReadKey();
        }

        static void PrintEntities() {
            foreach(Entity e in engine.GetEntities()) {
                PrintEntity(e);
            }
        }

        static void PrintEntity(Entity e) {
            Console.Write("Entity {0}: ", e.ID);
            foreach(IComponent comp in e.GetComponents()) {
                Console.Write(comp);
                Console.Write(", ");
            }
            Console.WriteLine();
        }

        static Entity Build() {
            Entity e = new Entity();
            PositionComponent pos = new PositionComponent();
            pos.X = rnd.Next(0, 100);
            pos.Y = rnd.Next(0, 100);
            e.Add(pos);
            VelocityComponent velo = new VelocityComponent();
            velo.X = rnd.Next(1, 10);
            velo.Y = rnd.Next(1, 10);
            e.Add(velo);
            return e;
        }
    }
}
