﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Signals;
using InfectedBytes.IronAshley.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace InfectedBytes.IronAshley.Core {
    public class Engine {
        private static SystemComparator systemComparator = new SystemComparator();
        private static Family empty = Family.All().Get();

        private List<Entity> entities;
        private ReadOnlyCollection<Entity> immutableEntities;
        private Dictionary<long, Entity> entitiesById;

        private List<EntityOperation> entityOperations;
        private EntityOperationPool entityOperationPool;

        private List<EntitySystem> systems;
        private ReadOnlyCollection<EntitySystem> immutableSystems;
        private Dictionary<Type, EntitySystem> systemsByClass;

        private Dictionary<Family, List<Entity>> families;
        private Dictionary<Family, ReadOnlyCollection<Entity>> immutableFamilies;

        private List<EntityListenerData> entityListeners;
        private EntityListenerData[] snapshotArray;
        private Dictionary<Family, Bits> entityListenerMasks;


        protected readonly IListener<Entity> componentAdded;
        protected readonly IListener<Entity> componentRemoved;

        private bool updating;

        private bool notifying;
        private long nextEntityId = 1;

        /** Mechanism to delay component addition/removal to avoid affecting system processing */
        private ComponentOperationPool componentOperationsPool;
        private List<ComponentOperation> componentOperations;
        private ComponentOperationHandler componentOperationHandler;

        public Engine() {
            entities = new List<Entity>(16);
            immutableEntities = new ReadOnlyCollection<Entity>(entities);
            entitiesById = new Dictionary<long, Entity>();
            entityOperations = new List<EntityOperation>(16);
            entityOperationPool = new EntityOperationPool();
            systems = new List<EntitySystem>(16);
            immutableSystems = new ReadOnlyCollection<EntitySystem>(systems);
            systemsByClass = new Dictionary<Type, EntitySystem>();
            families = new Dictionary<Family, List<Entity>>();
            immutableFamilies = new Dictionary<Family, ReadOnlyCollection<Entity>>();
            entityListeners = new List<EntityListenerData>(16);
            entityListenerMasks = new Dictionary<Family, Bits>();

            componentAdded = new ComponentListener(this);
            componentRemoved = new ComponentListener(this);

            updating = false;
            notifying = false;

            componentOperationsPool = new ComponentOperationPool();
            componentOperations = new List<ComponentOperation>();
            componentOperationHandler = new ComponentOperationHandler(this);
        }

        private long ObtainEntityId() {
            return nextEntityId++;
        }

        /// <summary>
        /// Adds an entity to this Engine.
        /// This will throw an IllegalArgumentException if the given entity
        /// was already registered with an engine.
        /// </summary>
        /// <param name="entity"></param>
        public void AddEntity(Entity entity) {
            if(entity.uuid != 0L) {
                throw new ArgumentException("Entity is already registered with an Engine id = " + entity.uuid);
            }

            entity.uuid = ObtainEntityId();
            if(updating || notifying) {
                EntityOperation operation = entityOperationPool.Obtain();
                operation.entity = entity;
                operation.type = EntityOperation.Type.Add;
                entityOperations.Add(operation);
            } else {
                AddEntityInternal(entity);
            }
        }

        /// <summary>
        /// Removes an entity from this Engine.
        /// </summary>
        /// <param name="entity"></param>
        public void RemoveEntity(Entity entity) {
            if(updating || notifying) {
                if(entity.scheduledForRemoval) {
                    return;
                }
                entity.scheduledForRemoval = true;
                EntityOperation operation = entityOperationPool.Obtain();
                operation.entity = entity;
                operation.type = EntityOperation.Type.Remove;
                entityOperations.Add(operation);
            } else {
                RemoveEntityInternal(entity);
            }
        }

        /// <summary>
        /// Removes all entities registered with this Engine.
        /// </summary>
        public void RemoveAllEntities() {
            if(updating || notifying) {
                foreach(Entity entity in entities) {
                    entity.scheduledForRemoval = true;
                }
                EntityOperation operation = entityOperationPool.Obtain();
                operation.type = EntityOperation.Type.RemoveAll;
                entityOperations.Add(operation);
            } else {
                while(entities.Count > 0) {
                    RemoveEntity(entities[0]);
                }
            }
        }

        /// <summary>
        /// Returns the Entity with the given id or null if those ID doesn't exist
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Entity GetEntity(long id) {
            if(entitiesById.ContainsKey(id)) return entitiesById[id];
            return null;
        }

        /// <summary>
        /// Returns a readonly collection of the stored entities
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<Entity> GetEntities() {
            return immutableEntities;
        }

        /// <summary>
        /// Adds the EntitySystem to this Engine.
        /// If the Engine already had a system of the same class,
        /// the new one will replace the old one.
        /// </summary>
        /// <param name="system"></param>
        public void AddSystem(EntitySystem system) {
            Type systemType = system.GetType();
            EntitySystem oldSytem = GetSystem(systemType);

            if(oldSytem != null) {
                RemoveSystem(oldSytem);
            }

            systems.Add(system);
            systemsByClass[systemType] = system;
            system.AddedToEngineInternal(this);

            systems.Sort(systemComparator);
        }

        /// <summary>
        /// Removes the EntitySystem from this Engine.
        /// </summary>
        /// <param name="system"></param>
        public void RemoveSystem(EntitySystem system) {
            if(systems.Remove(system)) {
                systemsByClass.Remove(system.GetType());
                system.RemovedFromEngineInternal(this);
            }
        }

        /// <summary>
        /// Removes the EntitySystem from this Engine.
        /// </summary>
        /// <typeparam name="T">EntitySystem to be removed</typeparam>
        public void RemoveSystem<T>() where T : EntitySystem {
            RemoveSystem(GetSystem<T>());
        }

        /// <summary>
        /// Quick EntitySystem retrieval.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetSystem<T>() where T : EntitySystem {
            if(systemsByClass.ContainsKey(typeof(T))) return (T)systemsByClass[typeof(T)];
            return null;
        }
        /// <summary>
        /// Quick EntitySystem retrieval.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public EntitySystem GetSystem(Type t) {
            if(systemsByClass.ContainsKey(t)) return systemsByClass[t];
            return null;
        }

        /// <summary>
        /// </summary>
        /// <returns>readonly collection of all entity systems managed by the Engine.</returns>
        public ReadOnlyCollection<EntitySystem> GetSystems() {
            return immutableSystems;
        }

        /// <summary>
        /// Returns immutable collection of entities for the specified Family. Will return the same instance every time.
        /// </summary>
        /// <param name="family"></param>
        /// <returns></returns>
        public ReadOnlyCollection<Entity> GetEntitiesFor(Family family) {
            return RegisterFamily(family);
        }

        /// <summary>
        /// Adds an IEntityListener.
        /// The listener will be notified every time an entity is added/removed to/from the engine.
        /// </summary>
        /// <param name="listener"></param>
        public void AddEntityListener(IEntityListener listener) {
            AddEntityListener(empty, 0, listener);
        }

        /// <summary>
        /// Adds an IEntityListener. The listener will be notified every time an entity is added/removed
        /// to/from the engine. The priority determines in which order the entity listeners will be called. Lower
        /// value means it will get executed first.
        /// </summary>
        /// <param name="priority"></param>
        /// <param name="listener"></param>
        public void AddEntityListener(int priority, IEntityListener listener) {
            AddEntityListener(empty, priority, listener);
        }

        /// <summary>
        /// Adds an IEntityListener for a specific Family.
        /// The listener will be notified every time an entity is added/removed to/from the given family.
        /// </summary>
        /// <param name="family"></param>
        /// <param name="listener"></param>
        public void AddEntityListener(Family family, IEntityListener listener) {
            AddEntityListener(family, 0, listener);
        }

        /// <summary>
        /// Adds an IEntityListener for a specific Family. The listener will be notified every time an entity is
        /// added/removed to/from the given family. The priority determines in which order the entity listeners will be called. Lower
        /// value means it will get executed first.
        /// </summary>
        /// <param name="family"></param>
        /// <param name="priority"></param>
        /// <param name="listener"></param>
        public void AddEntityListener(Family family, int priority, IEntityListener listener) {
            RegisterFamily(family);

            int insertionIndex = 0;
            while(insertionIndex < entityListeners.Count) {
                if(entityListeners[insertionIndex].Priority <= priority) {
                    insertionIndex++;
                } else {
                    break;
                }
            }

            // Shift up bitmasks by one step
            foreach(Bits mask in entityListenerMasks.Values) {
                for(int k = mask.Count; k > insertionIndex; k--) {
                    if(mask.Get(k - 1)) {
                        mask.Set(k);
                    } else {
                        mask.Clear(k);
                    }
                }
                mask.Clear(insertionIndex);
            }

            entityListenerMasks[family].Set(insertionIndex);

            EntityListenerData entityListenerData = new EntityListenerData();
            entityListenerData.Listener = listener;
            entityListenerData.Priority = priority;
            entityListeners.Insert(insertionIndex, entityListenerData);
        }

        /// <summary>
        /// Removes an IEntityListener
        /// </summary>
        /// <param name="listener"></param>
        public void RemoveEntityListener(IEntityListener listener) {
            for(int i = 0; i < entityListeners.Count; i++) {
                EntityListenerData entityListenerData = entityListeners[i];
                if(entityListenerData.Listener == listener) {
                    // Shift down bitmasks by one step
                    foreach(Bits mask in entityListenerMasks.Values) {
                        for(int k = i, n = mask.Count; k < n; k++) {
                            if(mask.Get(k + 1)) {
                                mask.Set(k);
                            } else {
                                mask.Clear(k);
                            }
                        }
                    }

                    entityListeners.RemoveAt(i--);
                }
            }
        }

        /// <summary>
        /// Updates all the systems in this Engine.
        /// </summary>
        /// <param name="deltaTime">The time passed since the last frame.</param>
        public void Update(float deltaTime) {
            if(updating) {
                throw new Exception("Cannot call update() on an Engine that is already updating.");
            }

            updating = true;
            for(int i = 0; i < systems.Count; i++) {
                EntitySystem system = systems[i];
                if(system.CheckProcessing()) {
                    system.Update(deltaTime);
                }

                ProcessComponentOperations();
                ProcessPendingEntityOperations();
            }

            updating = false;
        }

        private void UpdateFamilyMembership(Entity entity, bool removing) {
            // Find families that the entity was added to/removed from, and fill
            // the bitmasks with corresponding listener bits.
            Bits addListenerBits = new Bits();
            Bits removeListenerBits = new Bits();

            foreach(Family family in entityListenerMasks.Keys) {
                int familyIndex = family.Index;
                Bits entityFamilyBits = entity.GetFamilyBits();

                bool belongsToFamily = entityFamilyBits.Get(familyIndex);
                bool matches = family.Matches(entity) && !removing;

                if(belongsToFamily != matches) {
                    Bits listenersMask = entityListenerMasks[family];
                    List<Entity> familyEntities = families[family];
                    if(matches) {
                        addListenerBits.Or(listenersMask);
                        familyEntities.Add(entity);
                        entityFamilyBits.Set(familyIndex);
                    } else {
                        removeListenerBits.Or(listenersMask);
                        familyEntities.Remove(entity);
                        entityFamilyBits.Clear(familyIndex);
                    }
                }
            }

            // Notify listeners; set bits match indices of listeners
            notifying = true;
            //make snapshot
            if(snapshotArray == null || entityListeners.Count > snapshotArray.Length)
                snapshotArray = entityListeners.ToArray();
            else
                entityListeners.CopyTo(snapshotArray);

            for(int i = removeListenerBits.NextSetBit(0); i >= 0; i = removeListenerBits.NextSetBit(i + 1)) {
                snapshotArray[i].Listener.EntityRemoved(entity);
            }

            for(int i = addListenerBits.NextSetBit(0); i >= 0; i = addListenerBits.NextSetBit(i + 1)) {
                snapshotArray[i].Listener.EntityAdded(entity);
            }

            for(int i = 0; i < snapshotArray.Length; i++) snapshotArray[i] = null;
            notifying = false;
        }

        protected virtual void RemoveEntityInternal(Entity entity) {
            bool removed = false;

            entity.scheduledForRemoval = false;
            entities.Remove(entity);

            removed = entitiesById.Remove(entity.ID);

            UpdateFamilyMembership(entity, true);

            entity.ComponentAdded.Remove(componentAdded);
            entity.ComponentRemoved.Remove(componentRemoved);
            entity.componentOperationHandler = null;

            if(removed) {
                entity.uuid = 0L;
            }
        }

        protected void AddEntityInternal(Entity entity) {
            entities.Add(entity);
            entitiesById[entity.ID] = entity;

            UpdateFamilyMembership(entity, false);

            entity.ComponentAdded.Add(componentAdded);
            entity.ComponentRemoved.Add(componentRemoved);
            entity.componentOperationHandler = componentOperationHandler;
        }

        private ReadOnlyCollection<Entity> RegisterFamily(Family family) {
            ReadOnlyCollection<Entity> immutableEntitiesInFamily;

            if(!immutableFamilies.TryGetValue(family, out immutableEntitiesInFamily)) {
                List<Entity> familyEntities = new List<Entity>(16);
                immutableEntitiesInFamily = new ReadOnlyCollection<Entity>(familyEntities);
                families[family] = familyEntities;
                immutableFamilies[family] = immutableEntitiesInFamily;
                entityListenerMasks[family] = new Bits();

                foreach(Entity entity in this.entities) {
                    UpdateFamilyMembership(entity, false);
                }
            }

            return immutableEntitiesInFamily;
        }

        private void ProcessPendingEntityOperations() {
            while(entityOperations.Count > 0) {
                EntityOperation operation = entityOperations[entityOperations.Count - 1];
                entityOperations.RemoveAt(entityOperations.Count - 1);

                switch(operation.type) {
                    case EntityOperation.Type.Add: AddEntityInternal(operation.entity); break;
                    case EntityOperation.Type.Remove: RemoveEntityInternal(operation.entity); break;
                    case EntityOperation.Type.RemoveAll:
                        while(entities.Count > 0) {
                            RemoveEntityInternal(entities[0]);
                        }
                        break;
                }

                entityOperationPool.Free(operation);
            }

            entityOperations.Clear();
        }

        private void ProcessComponentOperations() {
            for(int i = 0; i < componentOperations.Count; ++i) {
                ComponentOperation operation = componentOperations[i];

                switch(operation.type) {
                    case ComponentOperation.Type.Add: operation.entity.AddInternal(operation.component); break;
                    case ComponentOperation.Type.Remove: operation.entity.RemoveInternal(operation.componentClass); break;
                    default: break;
                }

                componentOperationsPool.Free(operation);
            }

            componentOperations.Clear();
        }

        private class ComponentListener : IListener<Entity> {
            private Engine engine;

            public ComponentListener(Engine engine) {
                this.engine = engine;
            }

            public void Receive(Signal<Entity> signal, Entity obj) {
                engine.UpdateFamilyMembership(obj, false);
            }
        }

        internal class ComponentOperationHandler {
            private Engine engine;

            public ComponentOperationHandler(Engine engine) {
                this.engine = engine;
            }

            public void Add(Entity entity, IComponent component) {
                if(engine.updating) {
                    ComponentOperation operation = engine.componentOperationsPool.Obtain();
                    operation.MakeAdd(entity, component);
                    engine.componentOperations.Add(operation);
                } else {
                    entity.AddInternal(component);
                }
            }

            public void Remove<T>(Entity entity) {
                if(engine.updating) {
                    ComponentOperation operation = engine.componentOperationsPool.Obtain();
                    operation.MakeRemove(entity, typeof(T));
                    engine.componentOperations.Add(operation);
                } else {
                    entity.RemoveInternal(typeof(T));
                }
            }
        }

        private class ComponentOperation : Poolable {
            public enum Type {
                Add,
                Remove,
            }

            public Type type;
            public Entity entity;
            public IComponent component;
            public System.Type componentClass; // Class<? extends IComponent>

            public void MakeAdd(Entity entity, IComponent component) {
                this.type = Type.Add;
                this.entity = entity;
                this.component = component;
                this.componentClass = null;
            }

            public void MakeRemove(Entity entity, System.Type componentClass) {
                this.type = Type.Remove;
                this.entity = entity;
                this.component = null;
                this.componentClass = componentClass;
            }

            public void Reset() {
                entity = null;
                component = null;
            }
        }

        private class ComponentOperationPool : Pool<ComponentOperation> {
            protected override ComponentOperation NewObject() {
                return new ComponentOperation();
            }
        }

        private class EntityListenerData {
            public IEntityListener Listener;
            public int Priority;
        }

        private class SystemComparator : IComparer<EntitySystem> {
            public int Compare(EntitySystem a, EntitySystem b) {
                return a.Priority > b.Priority ? 1 : (a.Priority == b.Priority) ? 0 : -1;
            }
        }

        private class EntityOperation : Poolable {
            public enum Type {
                Add,
                Remove,
                RemoveAll
            }

            public Type type;
            public Entity entity;

            public void Reset() {
                entity = null;
            }
        }

        private class EntityOperationPool : Pool<EntityOperation> {
            protected override EntityOperation NewObject() {
                return new EntityOperation();
            }
        }
    }
}
