﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace InfectedBytes.IronAshley.Core {
    /// <summary>
    ///  Represents a group of IComponents. It is used to describe what Entity objects an EntitySystem should
    ///  process. Example: <code>Family.All(typeof(PositionComponent), typeof(VelocityComponent)).get()</code> Families can't be instantiated
    ///  directly but must be accessed via a builder (start with <code>Family.All()</code>, <code>Family.One()</code> or <code>Family.Exclude()</code>).
    ///  This is to avoid duplicate families that describe the same components.
    /// </summary>
    public class Family {
        private static Dictionary<String, Family> families = new Dictionary<String, Family>();
        private static int familyIndex = 0;
        private static readonly Builder builder = new Builder();
        private static readonly Bits zeroBits = new Bits();

        private readonly Bits all;
        private readonly Bits one;
        private readonly Bits exclude;
        private readonly int index;

        /// <summary>
        /// Private constructor, use static method Family.GetFamilyFor()
        /// </summary>
        /// <param name="all"></param>
        /// <param name="any"></param>
        /// <param name="exclude"></param>
        private Family(Bits all, Bits any, Bits exclude) {
            this.all = all;
            this.one = any;
            this.exclude = exclude;
            this.index = familyIndex++;
        }

        /// <summary>
        /// This family's unique index
        /// </summary>
        public int Index { get { return this.index; } }

        /// <summary>
        /// Whether the entity matches the family requirements or not
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Matches(Entity entity) {
            Bits entityComponentBits = entity.GetComponentBits();

            if(!entityComponentBits.ContainsAll(all)) {
                return false;
            }

            if(!one.IsEmpty && !one.Intersects(entityComponentBits)) {
                return false;
            }

            if(!exclude.IsEmpty && exclude.Intersects(entityComponentBits)) {
                return false;
            }

            return true;
        }

        #region All
        /// <summary>
        /// Shorthand for All(typeof(A))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static Builder All<A>() where A : IComponent {
            return builder.Reset().All(typeof(A));
        }
        /// <summary>
        /// Shorthand for All(typeof(A), typeof(B))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <returns></returns>
        public static Builder All<A, B>()
            where A : IComponent
            where B : IComponent {
            return builder.Reset().All(typeof(A), typeof(B));
        }
        /// <summary>
        /// Shorthand for All(typeof(A), typeof(B), typeof(C))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <typeparam name="C"></typeparam>
        /// <returns></returns>
        public static Builder All<A, B, C>()
            where A : IComponent
            where B : IComponent
            where C : IComponent {
            return builder.Reset().All(typeof(A), typeof(B), typeof(C));
        }
        /// <summary>
        /// Shorthand for All(typeof(A), typeof(B), typeof(C), typeof(D))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <typeparam name="C"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <returns></returns>
        public static Builder All<A, B, C, D>()
            where A : IComponent
            where B : IComponent
            where C : IComponent
            where D : IComponent {
            return builder.Reset().All(typeof(A), typeof(B), typeof(C), typeof(D));
        }

        /// <summary>
        /// </summary>
        /// <param name="componentTypes">entities will have to contain all of the specified components.</param>
        /// <returns>A Builder singleton instance to get a family</returns>
        public static Builder All(params Type[] componentTypes) {
            return builder.Reset().All(componentTypes);
        }
        #endregion

        #region One
        /// <summary>
        /// Shorthand for One(typeof(A))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static Builder One<A>() where A : IComponent {
            return builder.Reset().One(typeof(A));
        }
        /// <summary>
        /// Shorthand for One(typeof(A), typeof(B))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <returns></returns>
        public static Builder One<A, B>()
            where A : IComponent
            where B : IComponent {
            return builder.Reset().One(typeof(A), typeof(B));
        }
        /// <summary>
        /// Shorthand for One(typeof(A), typeof(B), typeof(C))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <typeparam name="C"></typeparam>
        /// <returns></returns>
        public static Builder One<A, B, C>()
            where A : IComponent
            where B : IComponent
            where C : IComponent {
            return builder.Reset().One(typeof(A), typeof(B), typeof(C));
        }
        /// <summary>
        /// Shorthand for One(typeof(A), typeof(B), typeof(C), typeof(D))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <typeparam name="C"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <returns></returns>
        public static Builder One<A, B, C, D>()
            where A : IComponent
            where B : IComponent
            where C : IComponent
            where D : IComponent {
            return builder.Reset().One(typeof(A), typeof(B), typeof(C), typeof(D));
        }
        /// <summary>
        /// </summary>
        /// <param name="componentTypes">entities will have to contain at least one of the specified components.</param>
        /// <returns>A Builder singleton instance to get a family</returns>
        public static Builder One(params Type[] componentTypes) {
            return builder.Reset().One(componentTypes);
        }
        #endregion

        #region Exclude
        /// <summary>
        /// Shorthand for Exclude(typeof(A))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static Builder Exclude<A>() where A : IComponent {
            return builder.Reset().Exclude(typeof(A));
        }
        /// <summary>
        /// Shorthand for Exclude(typeof(A), typeof(B))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <returns></returns>
        public static Builder Exclude<A, B>()
            where A : IComponent
            where B : IComponent {
            return builder.Reset().Exclude(typeof(A), typeof(B));
        }
        /// <summary>
        /// Shorthand for Exclude(typeof(A), typeof(B), typeof(C))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <typeparam name="C"></typeparam>
        /// <returns></returns>
        public static Builder Exclude<A, B, C>()
            where A : IComponent
            where B : IComponent
            where C : IComponent {
            return builder.Reset().Exclude(typeof(A), typeof(B), typeof(C));
        }
        /// <summary>
        /// Shorthand for Exclude(typeof(A), typeof(B), typeof(C), typeof(D))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <typeparam name="C"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <returns></returns>
        public static Builder Exclude<A, B, C, D>()
            where A : IComponent
            where B : IComponent
            where C : IComponent
            where D : IComponent {
            return builder.Reset().Exclude(typeof(A), typeof(B), typeof(C), typeof(D));
        }
        /// <summary>
        /// </summary>
        /// <param name="componentTypes">entities cannot contain any of the specified components.</param>
        /// <returns>A Builder singleton instance to get a family</returns>
        public static Builder Exclude(params Type[] componentTypes) {
            return builder.Reset().Exclude(componentTypes);
        }
        #endregion

        public class Builder {
            private static Bits zeroBits = new Bits();
            private Bits all = zeroBits;
            private Bits one = zeroBits;
            private Bits exclude = zeroBits;

            internal Builder() {

            }

            /// <summary>
            /// Resets the builder instance
            /// </summary>
            /// <returns>A Builder singleton instance to get a family</returns>
            public Builder Reset() {
                all = zeroBits;
                one = zeroBits;
                exclude = zeroBits;
                return this;
            }
            #region All
            /// <summary>
            /// Shorthand for All(typeof(A))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <returns></returns>
            public Builder All<A>() where A : IComponent {
                return All(typeof(A));
            }
            /// <summary>
            /// Shorthand for All(typeof(A), typeof(B))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <typeparam name="B"></typeparam>
            /// <returns></returns>
            public Builder All<A, B>()
                where A : IComponent
                where B : IComponent {
                return All(typeof(A), typeof(B));
            }
            /// <summary>
            /// Shorthand for All(typeof(A), typeof(B), typeof(C))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <typeparam name="B"></typeparam>
            /// <typeparam name="C"></typeparam>
            /// <returns></returns>
            public Builder All<A, B, C>()
                where A : IComponent
                where B : IComponent
                where C : IComponent {
                return All(typeof(A), typeof(B), typeof(C));
            }
            /// <summary>
            /// Shorthand for All(typeof(A), typeof(B), typeof(C), typeof(D))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <typeparam name="B"></typeparam>
            /// <typeparam name="C"></typeparam>
            /// <returns></returns>
            public Builder All<A, B, C, D>()
                where A : IComponent
                where B : IComponent
                where C : IComponent
                where D : IComponent {
                return All(typeof(A), typeof(B), typeof(C), typeof(D));
            }
            /// <summary>
            /// </summary>
            /// <param name="componentTypes">entities will have to contain all of the specified components.</param>
            /// <returns>A Builder singleton instance to get a family</returns>
            public Builder All(params Type[] componentTypes) {
                all = ComponentType.GetBitsFor(componentTypes);
                return this;
            }
            #endregion

            #region One
            /// <summary>
            /// Shorthand for One(typeof(A))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <returns></returns>
            public Builder One<A>() where A : IComponent {
                return One(typeof(A));
            }
            /// <summary>
            /// Shorthand for One(typeof(A), typeof(B))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <typeparam name="B"></typeparam>
            /// <returns></returns>
            public Builder One<A, B>()
                where A : IComponent
                where B : IComponent {
                return One(typeof(A), typeof(B));
            }
            /// <summary>
            /// Shorthand for One(typeof(A), typeof(B), typeof(C))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <typeparam name="B"></typeparam>
            /// <typeparam name="C"></typeparam>
            /// <returns></returns>
            public Builder One<A, B, C>()
                where A : IComponent
                where B : IComponent
                where C : IComponent {
                return One(typeof(A), typeof(B), typeof(C));
            }
            /// <summary>
            /// Shorthand for One(typeof(A), typeof(B), typeof(C), typeof(D))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <typeparam name="B"></typeparam>
            /// <typeparam name="C"></typeparam>
            /// <returns></returns>
            public Builder One<A, B, C, D>()
                where A : IComponent
                where B : IComponent
                where C : IComponent
                where D : IComponent {
                return One(typeof(A), typeof(B), typeof(C), typeof(D));
            }
            /// <summary>
            /// </summary>
            /// <param name="componentTypes">entities will have to contain at least one of the specified components.</param>
            /// <returns>A Builder singleton instance to get a family</returns>
            public Builder One(params Type[] componentTypes) {
                one = ComponentType.GetBitsFor(componentTypes);
                return this;
            }
            #endregion

            #region Exclude
            /// <summary>
            /// Shorthand for Exclude(typeof(A))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <returns></returns>
            public Builder Exclude<A>() where A : IComponent {
                return Exclude(typeof(A));
            }
            /// <summary>
            /// Shorthand for Exclude(typeof(A), typeof(B))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <typeparam name="B"></typeparam>
            /// <returns></returns>
            public Builder Exclude<A, B>()
                where A : IComponent
                where B : IComponent {
                return Exclude(typeof(A), typeof(B));
            }
            /// <summary>
            /// Shorthand for Exclude(typeof(A), typeof(B), typeof(C))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <typeparam name="B"></typeparam>
            /// <typeparam name="C"></typeparam>
            /// <returns></returns>
            public Builder Exclude<A, B, C>()
                where A : IComponent
                where B : IComponent
                where C : IComponent {
                return Exclude(typeof(A), typeof(B), typeof(C));
            }
            /// <summary>
            /// Shorthand for Exclude(typeof(A), typeof(B), typeof(C), typeof(D))
            /// </summary>
            /// <typeparam name="A"></typeparam>
            /// <typeparam name="B"></typeparam>
            /// <typeparam name="C"></typeparam>
            /// <returns></returns>
            public Builder Exclude<A, B, C, D>()
                where A : IComponent
                where B : IComponent
                where C : IComponent
                where D : IComponent {
                return Exclude(typeof(A), typeof(B), typeof(C), typeof(D));
            }
            /// <summary>
            /// </summary>
            /// <param name="componentTypes">entities cannot contain any of the specified components.</param>
            /// <returns>A Builder singleton instance to get a family</returns>
            public Builder Exclude(params Type[] componentTypes) {
                exclude = ComponentType.GetBitsFor(componentTypes);
                return this;
            }
            #endregion

            /// <summary>
            /// A family for the configured component types
            /// </summary>
            /// <returns></returns>
            public Family Get() {
                String hash = GetFamilyHash(all, one, exclude);
                Family family;
                families.TryGetValue(hash, out family);
                if(family == null) {
                    family = new Family(all, one, exclude);
                    families[hash] = family;
                }
                return family;
            }

            /// <summary>
            /// A family for the configured component types
            /// </summary>
            /// <param name="builder"></param>
            /// <returns></returns>
            public static implicit operator Family(Builder builder) {
                return builder.Get();
            }
        }

        public override int GetHashCode() {
            return index;
        }

        public override bool Equals(object obj) {
            return this == obj;
        }

        private static String GetFamilyHash(Bits all, Bits one, Bits exclude) {
            StringBuilder stringBuilder = new StringBuilder();
            if(!all.IsEmpty) {
                stringBuilder.Append("{all:").Append(GetBitsString(all)).Append("}");
            }
            if(!one.IsEmpty) {
                stringBuilder.Append("{one:").Append(GetBitsString(one)).Append("}");
            }
            if(!exclude.IsEmpty) {
                stringBuilder.Append("{exclude:").Append(GetBitsString(exclude)).Append("}");
            }
            return stringBuilder.ToString();
        }

        private static String GetBitsString(Bits bits) {
            StringBuilder stringBuilder = new StringBuilder();

            int numBits = bits.Count;
            for(int i = 0; i < numBits; ++i) {
                stringBuilder.Append(bits.Get(i) ? "1" : "0");
            }

            return stringBuilder.ToString();
        }
    }
}
