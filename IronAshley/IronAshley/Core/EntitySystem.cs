﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace InfectedBytes.IronAshley.Core {
    /// <summary>
    /// Abstract class for processing sets of Entity objects.
    /// </summary>
    public class EntitySystem {
        /// <summary>
        /// Use this to set the priority of the system. Lower means it'll get executed first.
        /// </summary>
        public int Priority;

        private bool processing;
        private Engine engine;

        /// <summary>
        /// Default constructor that will initialise an EntitySystem with priority 0.
        /// </summary>
        public EntitySystem() : this(0) { }

        /// <summary>
        /// Initialises the EntitySystem with the priority specified.
        /// </summary>
        /// <param name="priority">priority The priority to execute this system with (lower means higher priority).</param>
        public EntitySystem(int priority) {
            this.Priority = priority;
            this.processing = true;
        }

        /// <summary>
        /// Called when this EntitySystem is added to an {@link Engine}.
        /// </summary>
        /// <param name="engine">The Engine this system was added to.</param>
        public virtual void AddedToEngine(Engine engine) {
        }

        /// <summary>
        /// Called when this EntitySystem is removed from an {@link Engine}.
        /// </summary>
        /// <param name="engine">The Engine the system was removed from.</param>
        public virtual void RemovedFromEngine(Engine engine) {
        }

        /// <summary>
        /// The update method called every tick.
        /// </summary>
        /// <param name="deltaTime">The time passed since last frame in seconds.</param>
        public virtual void Update(float deltaTime) {
        }

        /// <summary>
        /// Whether or not the system should be processed.
        /// </summary>
        /// <returns></returns>
        public bool CheckProcessing() {
            return processing;
        }

        /// <summary>
        /// Sets whether or not the system should be processed by the Engine.
        /// </summary>
        /// <param name="processing"></param>
        public void SetProcessing(bool processing) {
            this.processing = processing;
        }

        /// <summary>
        /// It will be null if the system is not associated to any engine instance. */
        /// </summary>
        /// <returns>instance the system is registered to.</returns>
        public Engine GetEngine() {
            return engine;
        }

        internal void AddedToEngineInternal(Engine engine) {
            this.engine = engine;
            AddedToEngine(engine);
        }

        internal void RemovedFromEngineInternal(Engine engine) {
            this.engine = null;
            RemovedFromEngine(engine);
        }
    }
}
