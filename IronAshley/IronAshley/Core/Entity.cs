﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Signals;
using InfectedBytes.IronAshley.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace InfectedBytes.IronAshley.Core {
    /// <summary>
    /// Simple containers of IComponents that give them "data". The component's data is then processed by EntitySystems.
    /// </summary>
    public class Entity {
        /// <summary>
        /// A flag that can be used to bit mask this entity. Up to the user to manage.
        /// </summary>
        public int Flags;
        /// <summary>
        /// Will dispatch an event when a component is added.
        /// </summary>
        public readonly Signal<Entity> ComponentAdded;
        /// <summary>
        /// Will dispatch an event when a component is removed.
        /// </summary>
        public readonly Signal<Entity> ComponentRemoved;

        internal long uuid;
        internal bool scheduledForRemoval;
        internal Engine.ComponentOperationHandler componentOperationHandler;

        private Bag<IComponent> components;
        private List<IComponent> componentsArray;
        private ReadOnlyCollection<IComponent> immutableComponentsArray;
        private Bits componentBits;
        private Bits familyBits;

        /// <summary>
        /// Creates an empty Entity.
        /// </summary>
        public Entity() {
            components = new Bag<IComponent>();
            componentsArray = new List<IComponent>(16);
            immutableComponentsArray = new ReadOnlyCollection<IComponent>(componentsArray);
            componentBits = new Bits();
            familyBits = new Bits();
            Flags = 0;

            ComponentAdded = new Signal<Entity>();
            ComponentRemoved = new Signal<Entity>();
        }

        /// <summary>
        /// The Entity's unique id.
        /// </summary>
        public long ID { get { return uuid; } }

        /// <summary>
        /// Adds an IComponent to this Entity. If an IComponent of the same type already exists, it'll be replaced.
        /// </summary>
        /// <param name="component"></param>
        /// <returns>The Entity for easy chaining</returns>
        public Entity Add(IComponent component) {
            if(componentOperationHandler != null) {
                componentOperationHandler.Add(this, component);
            } else {
                AddInternal(component);
            }
            return this;
        }

        /// <summary>
        /// Removes the IComponent of the specified type. Since there is only ever one component of one type, 
        /// we don't need an instance reference.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>The removed IComponent, or null if the Entity did no contain such a component.</returns>
        public IComponent Remove<T>() where T : IComponent {
            ComponentType componentType = ComponentType.GetFor<T>();
            int componentTypeIndex = componentType.Index;
            IComponent removeComponent = components.Get(componentTypeIndex);

            if(componentOperationHandler != null) {
                componentOperationHandler.Remove<T>(this);
            } else {
                RemoveInternal(typeof(T));
            }

            return removeComponent;
        }

        /// <summary>
        /// Removes all the IComponent's from the Entity.
        /// </summary>
        public void RemoveAll() {
            while(componentsArray.Count > 0) {
                RemoveInternal(componentsArray[0].GetType());
            }
        }

        /// <summary>
        /// immutable collection with all the Entity IComponents.
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<IComponent> GetComponents() {
            return immutableComponentsArray;
        }

        /// <summary>
        /// Retrieve a component from this Entity by class. Note: the preferred way of retrieving IComponents is
        /// using ComponentMappers. This method is provided for convenience; using a ComponentMapper provides O(1) access to
        /// components while this method provides only O(logn).
        /// </summary>
        /// <typeparam name="T">the class of the component to be retrieved.</typeparam>
        /// <returns>the instance of the specified IComponent attached to this Entity, or null if no such IComponent exists.</returns>
        public IComponent GetComponent<T>() where T : IComponent {
            return GetComponent(ComponentType.GetFor<T>());
        }

        /// <summary>
        /// Retrieve a component from this Entity by class. Note: the preferred way of retrieving IComponents is
        /// using ComponentMappers. This method is provided for convenience; using a ComponentMapper provides O(1) access to
        /// components while this method provides only O(logn).
        /// </summary>
        /// <param name="type">the class of the component to be retrieved.</param>
        /// <returns>the instance of the specified IComponent attached to this Entity, or null if no such IComponent exists.</returns>
        public IComponent GetComponent(Type type) {
            return GetComponent(ComponentType.GetFor(type));
        }

        /// <summary>
        /// Internal use.
        /// </summary>
        /// <param name="componentType"></param>
        /// <returns>The IComponent object for the specified class, null if the Entity does not have any components for that class.</returns>
        public IComponent GetComponent(ComponentType componentType) {
            int componentTypeIndex = componentType.Index;

            if(componentTypeIndex < components.Capacity) {
                return components.Get(componentType.Index);
            } else {
                return null;
            }
        }

        /// <summary>
        /// Internal use.
        /// </summary>
        /// <param name="componentType"></param>
        /// <returns>Whether or not the Entity has a IComponent for the specified class.</returns>
        public bool HasComponent(ComponentType componentType) {
            return componentBits.Get(componentType.Index);
        }

        /// <summary>
        /// Internal use.
        /// </summary>
        /// <returns>This Entity's component bits, describing all the IComponents it contains.</returns>
        internal Bits GetComponentBits() {
            return componentBits;
        }

        /// <summary>
        /// This Entity's Family bits, describing all the EntitySystems it currently is being processed by.
        /// </summary>
        /// <returns></returns>
        internal Bits GetFamilyBits() {
            return familyBits;
        }

        internal Entity AddInternal(IComponent component) {
            Type componentClass = component.GetType();

            IComponent oldComponent = GetComponent(componentClass);

            if(component == oldComponent) {
                return this;
            }

            if(oldComponent != null) {
                RemoveInternal(componentClass);
            }

            int componentTypeIndex = ComponentType.GetIndexFor(componentClass);

            components.Set(componentTypeIndex, component);
            componentsArray.Add(component);

            componentBits.Set(componentTypeIndex);

            ComponentAdded.Dispatch(this);
            return this;
        }

        internal virtual IComponent RemoveInternal(Type componentClass) {
            ComponentType componentType = ComponentType.GetFor(componentClass);
            int componentTypeIndex = componentType.Index;
            IComponent removeComponent = components.Get(componentTypeIndex);

            if(removeComponent != null) {
                components.Set(componentTypeIndex, null);
                componentsArray.Remove(removeComponent);
                componentBits.Clear(componentTypeIndex);

                ComponentRemoved.Dispatch(this);
            }

            return removeComponent;
        }

        public override int GetHashCode() {
            return (int)(uuid ^ (uuid >> 32));
        }

        public override bool Equals(object obj) {
            if(this == obj) return true;
            if(!(obj is Entity)) return false;
            Entity other = (Entity)obj;
            return uuid == other.uuid;
        }

        /// <summary>
        /// true if the entity is scheduled to be removed
        /// </summary>
        /// <returns></returns>
        public bool IsScheduledForRemoval() {
            return scheduledForRemoval;
        }
    }
}
