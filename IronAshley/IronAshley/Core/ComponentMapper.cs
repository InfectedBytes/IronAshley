﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace InfectedBytes.IronAshley.Core {
    /// <summary>
    ///  Provides super fast IComponent retrieval from Entity objects.
    /// </summary>
    /// <typeparam name="T">the class type of the IComponent.</typeparam>
    public sealed class ComponentMapper<T> where T : IComponent {
        private readonly ComponentType componentType;

        /// <summary>
        /// Component class to be retrieved by the mapper.
        /// </summary>
        /// <returns>New instance that provides fast access to the IComponent of the specified class.</returns>
        public static ComponentMapper<T> Get() {
            return new ComponentMapper<T>();
        }

        /// <summary>
        /// The IComponent of the specified class belonging to entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public T Get(Entity entity) {
            return (T)entity.GetComponent(componentType);
        }

        /// <summary>
        /// Whether or not entity has the component of the specified class.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Has(Entity entity) {
            return entity.HasComponent(componentType);
        }

        private ComponentMapper() {
            componentType = ComponentType.GetFor<T>();
        }
    }
}
