﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace InfectedBytes.IronAshley.Core {
    /// <summary>
    /// Supports Entity and IComponent pooling. This improves performance in environments where creating/deleting
    /// entities is frequent as it greatly reduces memory allocation.
    /// Create entities using CreateEntity
    /// Create components using CreateComponent
    /// Components should implement the Poolable interface when in need to reset its state upon removal
    /// </summary>
    public class PooledEngine : Engine {

        private EntityPool entityPool;
        private ComponentPools componentPools;

        /// <summary>
        /// Creates a new PooledEngine with a maximum of 100 entities and 100 components of each type.
        /// </summary>
        public PooledEngine() : this(10, 100, 10, 100) { }

        /// <summary>
        /// Creates new PooledEngine with the specified pools size configurations.
        /// </summary>
        /// <param name="entityPoolInitialSize">initial number of pre-allocated entities.</param>
        /// <param name="entityPoolMaxSize">maximum number of pooled entities.</param>
        /// <param name="componentPoolInitialSize">initial size for each component type pool.</param>
        /// <param name="componentPoolMaxSize">maximum size for each component type pool.</param>
        public PooledEngine(int entityPoolInitialSize, int entityPoolMaxSize, int componentPoolInitialSize, int componentPoolMaxSize) {
            entityPool = new EntityPool(this, entityPoolInitialSize, entityPoolMaxSize);
            componentPools = new ComponentPools(componentPoolInitialSize, componentPoolMaxSize);
        }

        /// <summary>
        /// Clean Entity from the Engine pool. In order to add it to the Engine, use AddEntity(Entity).
        /// </summary>
        /// <returns></returns>
        public Entity CreateEntity() {
            return entityPool.Obtain();
        }

        /// <summary>
        /// Retrieves a new IComponent from the Engine pool. 
        /// It will be placed back in the pool whenever it's removedfrom an Entity or the Entity itself it's removed.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T CreateComponent<T>() {
            return componentPools.Obtain<T>();
        }

        /// <summary>
        /// Removes all free entities and components from their pools. Although this will likely result in garbage collection, it will free up memory.
        /// </summary>
        public void ClearPools() {
            entityPool.Clear();
            componentPools.Clear();
        }

        protected override void RemoveEntityInternal(Entity entity) {
            // Check if entity is able to be removed (id == 0 means either entity is not used by engine, or already removed/in pool)
            if(entity.ID == 0) return;
            base.RemoveEntityInternal(entity);
            if(entity is PooledEntity) {
                entityPool.Free((PooledEntity)entity);
            }
        }

        private class PooledEntity : Entity, Poolable {
            private PooledEngine engine;
            public PooledEntity(PooledEngine engine) {
                this.engine = engine;
            }
            internal override IComponent RemoveInternal(Type componentType) {
                IComponent component = base.RemoveInternal(componentType);

                if(component != null) {
                    engine.componentPools.Free(component);
                }

                return component;
            }

            public void Reset() {
                RemoveAll();
                uuid = 0L;
                Flags = 0;
                ComponentAdded.RemoveAllListeners();
                ComponentRemoved.RemoveAllListeners();
                scheduledForRemoval = false;
            }
        }

        private class EntityPool : Pool<PooledEntity> {
            private PooledEngine engine;

            public EntityPool(PooledEngine engine, int initialSize, int maxSize)
                : base(initialSize, maxSize) {
                this.engine = engine;
            }

            protected override PooledEntity NewObject() {
                return new PooledEntity(engine);
            }
        }

        private class ComponentPools {
            private Dictionary<Type, ReflectionPool> pools;
            private int initialSize;
            private int maxSize;

            public ComponentPools(int initialSize, int maxSize) {
                this.pools = new Dictionary<Type, ReflectionPool>();
                this.initialSize = initialSize;
                this.maxSize = maxSize;
            }

            public T Obtain<T>() {
                Type type = typeof(T);
                ReflectionPool pool;
                if(!pools.TryGetValue(type, out pool)) {
                    pool = new ReflectionPool(type, initialSize, maxSize);
                    pools[type] = pool;
                }

                return (T)pool.Obtain();
            }

            public void Free(object obj) {
                if(obj == null) {
                    throw new ArgumentException("object cannot be null.");
                }

                ReflectionPool pool;
                if(!pools.TryGetValue(obj.GetType(), out pool)) return;// Ignore freeing an object that was never retained.

                pool.Free(obj);
            }

            public void FreeAll(IList<object> objects) {
                if(objects == null) throw new ArgumentException("objects cannot be null.");

                for(int i = 0, n = objects.Count; i < n; i++) {
                    Object obj = objects[i];
                    if(obj == null) continue;
                    Free(obj);
                }
            }

            public void Clear() {
                foreach(ReflectionPool pool in pools.Values) {
                    pool.Clear();
                }
            }
        }
    }
}
