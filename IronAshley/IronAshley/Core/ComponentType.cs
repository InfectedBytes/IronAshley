﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace InfectedBytes.IronAshley.Core {
    /// <summary>
    ///  Uniquely identifies an IComponent sub-class. It assigns them an index which is used internally for fast comparison and
    ///  retrieval. See <see cref="Family"/> and <see cref="Entity"/>. You cannot instantiate a ComponentType. 
    ///  They can only be accessed via the different Get* methods. Each component class will always 
    ///  return the same instance of ComponentType.
    /// </summary>
    public sealed class ComponentType {
        private static Dictionary<Type, ComponentType> assignedComponenTypes = new Dictionary<Type, ComponentType>();
        private static int typeIndex = 0;

        /// <summary>
        /// This ComponentType's unique index
        /// </summary>
        public readonly int Index;
        private ComponentType() {
            Index = typeIndex++;
        }

        /// <summary>
        /// </summary>
        /// <typeparam name="T">The IComponent class</typeparam>
        /// <returns>A ComponentType matching the Component Class</returns>
        public static ComponentType GetFor<T>() where T : IComponent {
            ComponentType type;
            if(!assignedComponenTypes.TryGetValue(typeof(T), out type)) {
                type = new ComponentType();
                assignedComponenTypes[typeof(T)] = type;
            }
            return type;
        }

        /// <summary>
        /// </summary>
        /// <param name="t">The IComponent class</param>
        /// <returns>A ComponentType matching the Component Class</returns>
        public static ComponentType GetFor(Type t) {
            if(!(typeof(IComponent).IsAssignableFrom(t))) 
                throw new ArgumentException(t.Name + " is not an IComponent");
            ComponentType type;
            if(!assignedComponenTypes.TryGetValue(t, out type)) {
                type = new ComponentType();
                assignedComponenTypes[t] = type;
            }
            return type;
        }

        /// <summary>
        /// Quick helper method.
        /// </summary>
        /// <typeparam name="T">The IComponent class</typeparam>
        /// <returns>The index for the specified IComponent Class</returns>
        public static int GetIndexFor<T>() where T : IComponent {
            return GetFor<T>().Index;
        }

        /// <summary>
        /// Quick helper method.
        /// </summary>
        /// <param name="t">The IComponent class</param>
        /// <returns>The index for the specified IComponent Class</returns>
        public static int GetIndexFor(Type t) {
            return GetFor(t).Index;
        }

        /// <summary>
        /// Small helper method for easier use than GetBitsFor(typeof(A))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static Bits GetBitsFor<A>() where A : IComponent {
            Bits bits = new Bits();
            bits.Set(GetIndexFor<A>());
            return bits;
        }

        /// <summary>
        /// Small helper method for easier use than GetBitsFor(typeof(A), typeof(B))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <returns></returns>
        public static Bits GetBitsFor<A, B>()
            where A : IComponent
            where B : IComponent {
            Bits bits = new Bits();
            bits.Set(GetIndexFor<A>());
            bits.Set(GetIndexFor<B>());
            return bits;
        }

        /// <summary>
        /// Small helper method for easier use than GetBitsFor(typeof(A), typeof(B), typeof(C))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <typeparam name="C"></typeparam>
        /// <returns></returns>
        public static Bits GetBitsFor<A, B, C>()
            where A : IComponent
            where B : IComponent
            where C : IComponent {
            Bits bits = new Bits();
            bits.Set(GetIndexFor<A>());
            bits.Set(GetIndexFor<B>());
            bits.Set(GetIndexFor<C>());
            return bits;
        }

        /// <summary>
        /// Small helper method for easier use than GetBitsFor(typeof(A), typeof(B), typeof(C), typeof(D))
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <typeparam name="B"></typeparam>
        /// <typeparam name="C"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <returns></returns>
        public static Bits GetBitsFor<A, B, C, D>()
            where A : IComponent
            where B : IComponent
            where C : IComponent 
            where D : IComponent {
            Bits bits = new Bits();
            bits.Set(GetIndexFor<A>());
            bits.Set(GetIndexFor<B>());
            bits.Set(GetIndexFor<C>());
            bits.Set(GetIndexFor<D>());
            return bits;
        }

        /// <summary>
        /// </summary>
        /// <param name="types">list of IComponent classes</param>
        /// <returns>Bits representing the collection of components for quick comparison and matching.</returns>
        public static Bits GetBitsFor(params Type[] types) {
            Bits bits = new Bits();

            int typesLength = types.Length;
            for(int i = 0; i < typesLength; i++) {
                bits.Set(ComponentType.GetFor(types[i]).Index);
            }

            return bits;
        }

        public override int GetHashCode() {
            return Index;
        }

        public override bool Equals(object obj) {
            if(this == obj) return true;
            if(obj == null) return false;
            if(GetType() != obj.GetType()) return false;
            ComponentType other = (ComponentType)obj;
            return Index == other.Index;
        }
    }
}