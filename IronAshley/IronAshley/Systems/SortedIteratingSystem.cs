﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace InfectedBytes.IronAshley.Systems {
    /// <summary>
    /// A simple EntitySystem that processes each entity of a given family in the order specified by a comparator and calls
    /// processEntity() for each entity every time the EntitySystem is updated. This is really just a convenience class as rendering
    /// systems tend to iterate over a list of entities in a sorted manner. Adding entities will cause the entity list to be resorted.
    /// Call forceSort() if you changed your sorting criteria.
    /// </summary>
    public abstract class SortedIteratingSystem : EntitySystem, IEntityListener {
        private Family family;
        private List<Entity> sortedEntities;
        private readonly ReadOnlyCollection<Entity> entities;
        private bool shouldSort;
        private Comparer<Entity> comparator;

        /// <summary>
        /// Instantiates a system that will iterate over the entities described by the Family.
        /// </summary>
        /// <param name="family">The family of entities iterated over in this System</param>
        /// <param name="comparator">The comparator to sort the entities</param>
        public SortedIteratingSystem(Family family, Comparer<Entity> comparator) : this(family, comparator, 0) { }

        /// <summary>
        /// Instantiates a system that will iterate over the entities described by the Family, with a specific priority.
        /// </summary>
        /// <param name="family">The family of entities iterated over in this System</param>
        /// <param name="comparator">The comparator to sort the entities</param>
        /// <param name="priority">The priority to execute this system with (lower means higher priority)</param>
        public SortedIteratingSystem(Family family, Comparer<Entity> comparator, int priority)
            : base(priority) {
            this.family = family;
            sortedEntities = new List<Entity>(16);
            entities = new ReadOnlyCollection<Entity>(sortedEntities);
            this.comparator = comparator;
        }

        /// <summary>
        /// Call this if the sorting criteria have changed. The actual sorting will be delayed until the entities are processed.
        /// </summary>
        public void ForceSort() {
            shouldSort = true;
        }

        private void Sort() {
            if(shouldSort) {
                sortedEntities.Sort(comparator);
                shouldSort = false;
            }
        }


        public override void AddedToEngine(Engine engine) {
            ReadOnlyCollection<Entity> newEntities = engine.GetEntitiesFor(family);
            sortedEntities.Clear();
            if(newEntities.Count > 0) {
                for(int i = 0; i < newEntities.Count; ++i) {
                    sortedEntities.Add(newEntities[i]);
                }
                sortedEntities.Sort(comparator);
            }
            shouldSort = false;
            engine.AddEntityListener(family, this);
        }


        public override void RemovedFromEngine(Engine engine) {
            engine.RemoveEntityListener(this);
            sortedEntities.Clear();
            shouldSort = false;
        }


        public void EntityAdded(Entity entity) {
            sortedEntities.Add(entity);
            shouldSort = true;
        }


        public void EntityRemoved(Entity entity) {
            sortedEntities.Remove(entity);
            shouldSort = true;
        }


        public override void Update(float deltaTime) {
            Sort();
            for(int i = 0; i < sortedEntities.Count; ++i) {
                ProcessEntity(sortedEntities[i], deltaTime);
            }
        }

        /// <summary>
        /// set of entities processed by the system
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<Entity> GetEntities() {
            Sort();
            return entities;
        }

        /// <summary>
        /// the Family used when the system was created
        /// </summary>
        /// <returns></returns>
        public Family GetFamily() {
            return family;
        }

        /// <summary>
        /// This method is called on every entity on every update call of the EntitySystem. Override this to implement your system's specific processing.
        /// </summary>
        /// <param name="entity">The current Entity being processed</param>
        /// <param name="deltaTime">The delta time between the last and current frame</param>
        protected abstract void ProcessEntity(Entity entity, float deltaTime);
    }
}
