﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace InfectedBytes.IronAshley.Systems {
    /// <summary>
    /// A simple EntitySystem that processes a Family of entities not once per frame, but after a given interval.
    /// Entity processing logic should be placed in IntervalIteratingSystem.ProcessEntity(Entity).
    /// </summary>
    public abstract class IntervalIteratingSystem : IntervalSystem {
        private Family family;
        private ReadOnlyCollection<Entity> entities;

        /// <summary>
        /// </summary>
        /// <param name="family">represents the collection of family the system should process</param>
        /// <param name="interval">time in seconds between calls to IntervalIteratingSystem.UpdateInterval().</param>
        public IntervalIteratingSystem(Family family, float interval) : this(family, interval, 0) { }

        /// <summary>
        /// </summary>
        /// <param name="family">represents the collection of family the system should process</param>
        /// <param name="interval">time in seconds between calls to IntervalIteratingSystem.UpdateInterval().</param>
        /// <param name="priority"></param>
        public IntervalIteratingSystem(Family family, float interval, int priority)
            : base(interval, priority) {
            this.family = family;
        }

        public override void AddedToEngine(Engine engine) {
            entities = engine.GetEntitiesFor(family);
        }

        protected override void UpdateInterval() {
            for(int i = 0; i < entities.Count; ++i) {
                ProcessEntity(entities[i]);
            }
        }

        /// <summary>
        /// set of entities processed by the system
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<Entity> GetEntities() {
            return entities;
        }

        /// <summary>
        /// the Family used when the system was created
        /// </summary>
        /// <returns></returns>
        public Family GetFamily() {
            return family;
        }

        /// <summary>
        /// The user should place the entity processing logic here.
        /// </summary>
        /// <param name="entity"></param>
        protected abstract void ProcessEntity(Entity entity);
    }
}
