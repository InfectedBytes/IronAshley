﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace InfectedBytes.IronAshley.Systems {
    /// <summary>
    /// A simple EntitySystem that iterates over each entity and calls processEntity() for each entity every time the EntitySystem is 
    /// updated. This is really just a convenience class as most systems iterate over a list of entities.
    /// </summary>
    public abstract class IteratingSystem : EntitySystem {
        private Family family;
        private ReadOnlyCollection<Entity> entities;

        /// <summary>
        /// Instantiates a system that will iterate over the entities described by the Family.
        /// </summary>
        /// <param name="family">The family of entities iterated over in this System</param>
        public IteratingSystem(Family family) : this(family, 0) { }

        /// <summary>
        /// Instantiates a system that will iterate over the entities described by the Family, with a specific priority.
        /// </summary>
        /// <param name="family">The family of entities iterated over in this System</param>
        /// <param name="priority">The priority to execute this system with (lower means higher priority)</param>
        public IteratingSystem(Family family, int priority)
            : base(priority) {
            this.family = family;
        }

        public override void AddedToEngine(Engine engine) {
            entities = engine.GetEntitiesFor(family);
        }

        public override void RemovedFromEngine(Engine engine) {
            entities = null;
        }

        public override void Update(float deltaTime) {
            for(int i = 0; i < entities.Count; ++i) {
                ProcessEntity(entities[i], deltaTime);
            }
        }

        /// <summary>
        /// set of entities processed by the system
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<Entity> GetEntities() {
            return entities;
        }

        /// <summary>
        /// the Family used when the system was created
        /// </summary>
        /// <returns></returns>
        public Family GetFamily() {
            return family;
        }

        /// <summary>
        /// This method is called on every entity on every update call of the EntitySystem. Override this to implement your system's 
        /// specific processing.
        /// </summary>
        /// <param name="entity">The current Entity being processed</param>
        /// <param name="deltaTime">The delta time between the last and current frame</param>
        protected abstract void ProcessEntity(Entity entity, float deltaTime);
    }
}
