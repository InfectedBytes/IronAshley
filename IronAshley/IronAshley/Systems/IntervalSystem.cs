﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using InfectedBytes.IronAshley.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace InfectedBytes.IronAshley.Systems {
    /// <summary>
    /// A simple {@link EntitySystem} that does not run its update logic every call to EntitySystem.Update(float), but after a
    /// given interval. The actual logic should be placed in IntervalSystem.UpdateInterval().
    /// </summary>
    public abstract class IntervalSystem : EntitySystem {
        private float interval;
        private float accumulator;

        /// <summary>
        /// </summary>
        /// <param name="interval">time in seconds between calls to IntervalSystem.updateInterval().</param>
        public IntervalSystem(float interval) : this(interval, 0) { }


        /// <summary>
        /// </summary>
        /// <param name="interval">time in seconds between calls to IntervalSystem#updateInterval().</param>
        /// <param name="priority"></param>
        public IntervalSystem(float interval, int priority)
            : base(priority) {
            this.interval = interval;
            this.accumulator = 0;
        }

        public sealed override void Update(float deltaTime) {
            accumulator += deltaTime;

            while(accumulator >= interval) {
                accumulator -= interval;
                UpdateInterval();
            }
        }

        /// <summary>
        /// The processing logic of the system should be placed here.
        /// </summary>
        protected abstract void UpdateInterval();
    }
}
