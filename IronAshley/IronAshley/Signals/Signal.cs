﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace InfectedBytes.IronAshley.Signals {
    /// <summary>
    ///  A Signal is a basic event class then can dispatch an event to multiple listeners. It uses generics to allow any type of object
    ///  to be passed around on dispatch.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Signal<T> {
        private List<IListener<T>> listeners = new List<IListener<T>>();
        private IListener<T>[] snapshot;

        public Signal() { }

        /// <summary>
        /// Add a Listener to this Signal
        /// </summary>
        /// <param name="listener">The Listener to be added</param>
        public void Add(IListener<T> listener) {
            listeners.Add(listener);
        }

        /// <summary>
        /// Remove a listener from this Signal
        /// </summary>
        /// <param name="listener">The Listener to remove</param>
        public void Remove(IListener<T> listener) {
            listeners.Remove(listener);
        }

        /// <summary>
        /// Removes all listeners attached to this {@link Signal}.
        /// </summary>
        public void RemoveAllListeners() {
            listeners.Clear();
        }

        /// <summary>
        /// Dispatches an event to all Listeners registered to this Signal
        /// </summary>
        /// <param name="obj">The object to send off</param>
        public void Dispatch(T obj) {
            int n = listeners.Count;
            if(snapshot == null || snapshot.Length < n)
                snapshot = listeners.ToArray();
            else
                listeners.CopyTo(snapshot);

            for(int i = 0; i < n; i++) {
                snapshot[i].Receive(this, obj);
                snapshot[i] = null;
            }
        }
    }
}
