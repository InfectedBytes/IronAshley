﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace InfectedBytes.IronAshley.Utils {
    /// <summary>
    /// A pool of objects that can be reused to avoid allocation.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Pool<T> {
        /// <summary>
        /// The maximum number of objects that will be pooled.
        /// </summary>
        public readonly int Max;
        /// <summary>
        /// The highest number of free objects. Can be reset any time.
        /// </summary>
        public int Peak { get; set; }

        private List<T> freeObjects;

        /// <summary>
        /// Creates a pool with an initial capacity of 16 and no maximum.
        /// </summary>
        public Pool() : this(16, int.MaxValue) { }

        /// <summary>
        /// Creates a pool with the specified initial capacity and no maximum.
        /// </summary>
        /// <param name="initialCapacity"></param>
        public Pool(int initialCapacity) : this(initialCapacity, int.MaxValue) { }

        /// <summary>
        /// </summary>
        /// <param name="initialCapacity"></param>
        /// <param name="max">The maximum number of free objects to store in this pool.</param>
        public Pool(int initialCapacity, int max) {
            freeObjects = new List<T>(initialCapacity);
            this.Max = max;
        }

        abstract protected T NewObject();

        /// <summary>
        /// Returns an object from this pool. The object may be new (from NewObject()) or reused (previously Free(Object) freed).
        /// </summary>
        /// <returns></returns>
        public T Obtain() {
            if(freeObjects.Count == 0) return NewObject();
            T obj = freeObjects[freeObjects.Count - 1];
            return obj;
        }

       /// <summary>
       ///  Puts the specified object in the pool, making it eligible to be returned by Obtain(). 
       ///  If the pool already contains max free objects, the specified object is reset but not added to the pool.
       /// </summary>
       /// <param name="obj"></param>
        public void Free(T obj) {
            if(obj == null) throw new ArgumentException("object cannot be null.");
            if(freeObjects.Count < Max) {
                freeObjects.Add(obj);
                Peak = Math.Max(Peak, freeObjects.Count);
            }
            if(obj is Poolable) ((Poolable)obj).Reset();
        }

        /// <summary>
        /// Puts the specified objects in the pool. Null objects within the array are silently ignored.
        /// </summary>
        /// <param name="objects"></param>
        public void FreeAll(IList<T> objects) {
            if(objects == null) throw new ArgumentException("object cannot be null.");
            List<T> freeObjects = this.freeObjects;
            int max = this.Max;
            for(int i = 0; i < objects.Count; i++) {
                T obj = objects[i];
                if(obj == null) continue;
                if(freeObjects.Count < max) freeObjects.Add(obj);
                if(obj is Poolable) ((Poolable)obj).Reset();
            }
            Peak = Math.Max(Peak, freeObjects.Count);
        }

        /// <summary>
        /// Removes all free objects from this pool.
        /// </summary>
        public void Clear() {
            freeObjects.Clear();
        }

        /// <summary>
        /// The number of objects available to be obtained.
        /// </summary>
        /// <returns></returns>
        public int GetFree() {
            return freeObjects.Count;
        }
    }

    /// <summary>
    /// Objects implementing this interface will have Reset() called when passed to Free(Object).
    /// </summary>
    public interface Poolable {
        /// <summary>
        /// Resets the object for reuse. Object references should be nulled and fields may be set to default values.
        /// </summary>
        void Reset();
    }
}
