﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace InfectedBytes.IronAshley.Utils {
    public class ReflectionPool : Pool<object> {
        private readonly ConstructorInfo constructor;

        public ReflectionPool(Type type) : this(type, 16, int.MaxValue) { }

        public ReflectionPool(Type type, int initialCapacity) : this(type, initialCapacity, int.MaxValue) { }

        public ReflectionPool(Type type, int initialCapacity, int max)
            : base(initialCapacity, max) {
            constructor = type.GetConstructor(Type.EmptyTypes);
            if(constructor == null)
                throw new Exception("Class cannot be created (missing no-arg constructor): " + type.Name);
        }

        protected override object NewObject() {
            try {
                return constructor.Invoke(null);
            } catch(Exception ex) {
                throw new Exception("Unable to create new instance: " + constructor.DeclaringType.Name, ex);
            }
        }
    }
}
