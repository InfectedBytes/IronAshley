﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace InfectedBytes.IronAshley.Utils {
    /// <summary>
    /// A bitset, without size limitation, allows comparison via bitwise operators to other bitfields.
    /// </summary>
    public class Bits {

        private long[] bits = { 0 };

        public Bits() {
        }


        /// <summary>
        /// Creates a bit set whose initial size is large enough to explicitly represent bits with indices in the range 0 through nbits-1.
        /// </summary>
        /// <param name="nbits">Initial size of this bit set</param>
        public Bits(int nbits) {
            CheckCapacity(nbits >> 6);
        }

        public bool this[int index] {
            get { return Get(index); }
            set { if(value) Set(index); else Clear(index); }
        }

        /// <summary>
        /// Returns the bit value at the given index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool Get(int index) {
            int word = index >> 6;
            if(word >= bits.Length) return false;
            return (bits[word] & (1L << (index & 0x3F))) != 0L;
        }

        /// <summary>
        /// Returns the bit at the given index and clears it in one go.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool GetAndClear(int index) {
            int word = index >> 6;
            if(word >= bits.Length) return false;
            long oldBits = bits[word];
            bits[word] &= ~(1L << (index & 0x3F));
            return bits[word] != oldBits;
        }

        /// <summary>
        /// Returns the bit at the given index and sets it in one go.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool GetAndSet(int index) {
            int word = index >> 6;
            CheckCapacity(word);
            long oldBits = bits[word];
            bits[word] |= 1L << (index & 0x3F);
            return bits[word] == oldBits;
        }

        /// <summary>
        /// Set the bit at the given index to true.
        /// </summary>
        /// <param name="index"></param>
        public void Set(int index) {
            int word = index >> 6;
            CheckCapacity(word);
            bits[word] |= 1L << (index & 0x3F);
        }

        /// <summary>
        /// Flips the bit at the given index.
        /// </summary>
        /// <param name="index"></param>
        public void Flip(int index) {
            int word = index >> 6;
            CheckCapacity(word);
            bits[word] ^= 1L << (index & 0x3F);
        }

        private void CheckCapacity(int len) {
            if(len >= bits.Length) {
                long[] newBits = new long[len + 1];
                bits.CopyTo(newBits, 0);
                bits = newBits;
            }
        }

        /// <summary>
        /// Clears the bit at the given index.
        /// </summary>
        /// <param name="index"></param>
        public void Clear(int index) {
            int word = index >> 6;
            if(word >= bits.Length) return;
            bits[word] &= ~(1L << (index & 0x3F));
        }

        /// <summary>
        /// Clears the entire bit set.
        /// </summary>
        public void Clear() {
            long[] bits = this.bits;
            int length = bits.Length;
            for(int i = 0; i < length; i++) {
                bits[i] = 0L;
            }
        }

        /// <summary>
        /// Returns the number of bits currently stored, not the highset set bit!
        /// </summary>
        /// <returns></returns>
        public int NumBits {
            get {
                return bits.Length << 6;
            }
        }

        /// <summary>
        /// Returns the "logical size" of this bitset: 
        /// the index of the highest set bit in the bitset plus one. Returns zero if the bitset contains no set bits.
        /// </summary>
        public int Count {
            get {
                long[] bits = this.bits;
                for(int word = bits.Length - 1; word >= 0; --word) {
                    long bitsAtWord = bits[word];
                    if(bitsAtWord != 0) {
                        for(int bit = 63; bit >= 0; --bit) {
                            if((bitsAtWord & (1L << (bit & 0x3F))) != 0L) {
                                return (word << 6) + bit + 1;
                            }
                        }
                    }
                }
                return 0;
            }
        }

        /// <summary>
        /// Returns true if this bitset contains no bits that are set to true
        /// </summary>
        public bool IsEmpty {
            get {
                long[] bits = this.bits;
                int length = bits.Length;
                for(int i = 0; i < length; i++) {
                    if(bits[i] != 0L) {
                        return false;
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Returns the index of the first bit that is set to true that occurs on or after the specified starting index. 
        /// If no such bit exists then -1 is returned.
        /// </summary>
        /// <param name="fromIndex"></param>
        /// <returns></returns>
        public int NextSetBit(int fromIndex) {
            long[] bits = this.bits;
            int word = fromIndex >> 6;
            int bitsLength = bits.Length;
            if(word >= bitsLength) return -1;
            long bitsAtWord = bits[word];
            if(bitsAtWord != 0) {
                for(int i = fromIndex & 0x3f; i < 64; i++) {
                    if((bitsAtWord & (1L << (i & 0x3F))) != 0L) {
                        return (word << 6) + i;
                    }
                }
            }
            for(word++; word < bitsLength; word++) {
                if(word != 0) {
                    bitsAtWord = bits[word];
                    if(bitsAtWord != 0) {
                        for(int i = 0; i < 64; i++) {
                            if((bitsAtWord & (1L << (i & 0x3F))) != 0L) {
                                return (word << 6) + i;
                            }
                        }
                    }
                }
            }
            return -1;
        }

        /// <summary>
        /// Returns the index of the first bit that is set to false that occurs on or after the specified starting index.
        /// </summary>
        /// <param name="fromIndex"></param>
        /// <returns></returns>
        public int NextClearBit(int fromIndex) {
            long[] bits = this.bits;
            int word = fromIndex >> 6;
            int bitsLength = bits.Length;
            if(word >= bitsLength) return bits.Length << 6;
            long bitsAtWord = bits[word];
            for(int i = fromIndex & 0x3f; i < 64; i++) {
                if((bitsAtWord & (1L << (i & 0x3F))) == 0L) {
                    return (word << 6) + i;
                }
            }
            for(word++; word < bitsLength; word++) {
                if(word == 0) {
                    return word << 6;
                }
                bitsAtWord = bits[word];
                for(int i = 0; i < 64; i++) {
                    if((bitsAtWord & (1L << (i & 0x3F))) == 0L) {
                        return (word << 6) + i;
                    }
                }
            }
            return bits.Length << 6;
        }

        /// <summary>
        /// Performs a logical AND of this target bit set with the argument bit set. This bit set is modified so that each bit in
        /// it has the value true if and only if it both initially had the value true and the corresponding bit in the bit set argument
        /// also had the value true.
        /// </summary>
        /// <param name="other"></param>
        public void And(Bits other) {
            int commonWords = Math.Min(bits.Length, other.bits.Length);
            for(int i = 0; i < commonWords; i++) {
                bits[i] &= other.bits[i];
            }

            if(bits.Length > commonWords) {
                for(int i = commonWords, s = bits.Length; s > i; i++) {
                    bits[i] = 0L;
                }
            }
        }

        /// <summary>
        /// Performs a logical AND of the given bit sets. 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Bits operator &(Bits a, Bits b) {
            Bits result = new Bits(Math.Min(a.NumBits, b.NumBits));
            int commonWords = Math.Min(a.bits.Length, b.bits.Length);
            for(int i = 0; i < commonWords; i++) {
                result.bits[i] = a.bits[i] & b.bits[i];
            }
            return result;
        }

        /// <summary>
        /// Clears all of the bits in this bit set whose corresponding bit is set in the specified bit set.
        /// </summary>
        /// <param name="other"></param>
        public void AndNot(Bits other) {
            for(int i = 0, j = bits.Length, k = other.bits.Length; i < j && i < k; i++) {
                bits[i] &= ~other.bits[i];
            }
        }

        /// <summary>
        /// Performs a logical OR of this bit set with the bit set argument. This bit set is modified so that a bit in it has the
        /// value true if and only if it either already had the value true or the corresponding bit in the bit set argument has the
        /// value true.
        /// </summary>
        /// <param name="other"></param>
        public void Or(Bits other) {
            int commonWords = Math.Min(bits.Length, other.bits.Length);
            for(int i = 0; commonWords > i; i++) {
                bits[i] |= other.bits[i];
            }

            if(commonWords < other.bits.Length) {
                CheckCapacity(other.bits.Length);
                for(int i = commonWords, s = other.bits.Length; s > i; i++) {
                    bits[i] = other.bits[i];
                }
            }
        }

        /// <summary>
        /// Performs a logical OR of the given bit sets. 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Bits operator |(Bits a, Bits b) {
            Bits result = new Bits(Math.Max(a.NumBits, b.NumBits));
            int commonWords = Math.Min(a.bits.Length, b.bits.Length);
            for(int i = 0; commonWords > i; i++) {
                result.bits[i] = a.bits[i] | b.bits[i];
            }

            if(a.bits.Length > commonWords) {
                for(int i = commonWords; i < a.bits.Length; i++) {
                    result.bits[i] = a.bits[i];
                }
            } else if(b.bits.Length > commonWords) {
                for(int i = commonWords; i < b.bits.Length; i++) {
                    result.bits[i] = b.bits[i];
                }
            }
            return result;
        }

        /// <summary>
        /// Performs a logical XOR of this bit set with the bit set argument.
        /// </summary>
        /// <param name="other"></param>
        public void Xor(Bits other) {
            int commonWords = Math.Min(bits.Length, other.bits.Length);

            for(int i = 0; commonWords > i; i++) {
                bits[i] ^= other.bits[i];
            }

            if(commonWords < other.bits.Length) {
                CheckCapacity(other.bits.Length);
                for(int i = commonWords, s = other.bits.Length; s > i; i++) {
                    bits[i] = other.bits[i];
                }
            }
        }

        /// <summary>
        /// Performs a logical XOR of this bit set with the bit set argument.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Bits operator ^(Bits a, Bits b) {
            Bits result = new Bits(Math.Max(a.NumBits, b.NumBits));
            int commonWords = Math.Min(a.bits.Length, b.bits.Length);
            for(int i = 0; commonWords > i; i++) {
                result.bits[i] = a.bits[i] ^ b.bits[i];
            }

            if(a.bits.Length > commonWords) {
                for(int i = commonWords; i < a.bits.Length; i++) {
                    result.bits[i] = a.bits[i] ^ 0;
                }
            } else if(b.bits.Length > commonWords) {
                for(int i = commonWords; i < b.bits.Length; i++) {
                    result.bits[i] = b.bits[i] ^ 0;
                }
            }
            return result;
        }

        /// <summary>
        /// Returns true if the specified BitSet has any bits set to true that are also set to true in this BitSet.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Intersects(Bits other) {
            long[] bits = this.bits;
            long[] otherBits = other.bits;
            for(int i = Math.Min(bits.Length, otherBits.Length) - 1; i >= 0; i--) {
                if((bits[i] & otherBits[i]) != 0) {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns true if this bit set is a super set of the specified set, i.e. it has all bits set to true that are also set to true in the specified BitSet.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool ContainsAll(Bits other) {
            long[] bits = this.bits;
            long[] otherBits = other.bits;
            int otherBitsLength = otherBits.Length;
            int bitsLength = bits.Length;

            for(int i = bitsLength; i < otherBitsLength; i++) {
                if(otherBits[i] != 0) {
                    return false;
                }
            }
            for(int i = Math.Min(bitsLength, otherBitsLength) - 1; i >= 0; i--) {
                if((bits[i] & otherBits[i]) != otherBits[i]) {
                    return false;
                }
            }
            return true;
        }

        public override int GetHashCode() {
            int word = this.Count >> 6;
            int hash = 0;
            for(int i = 0; word >= i; i++) {
                hash = 127 * hash + (int)(bits[i] ^ (bits[i] >> 32));
            }
            return hash;
        }

        public override bool Equals(object obj) {
            if(this == obj)
                return true;
            if(obj == null)
                return false;
            if(GetType() != obj.GetType())
                return false;

            Bits other = (Bits)obj;
            long[] otherBits = other.bits;

            int commonWords = Math.Min(bits.Length, otherBits.Length);
            for(int i = 0; commonWords > i; i++) {
                if(bits[i] != otherBits[i])
                    return false;
            }

            if(bits.Length == otherBits.Length)
                return true;

            return Count == other.Count;
        }
    }
}
