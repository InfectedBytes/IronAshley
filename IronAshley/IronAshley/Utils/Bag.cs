﻿#region Copyright & License Information
/*******************************************************************************
 * Copyright 2015 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace InfectedBytes.IronAshley.Utils {

    /// <summary>
    /// Fast collection similar to Array that grows on demand as elements are accessed. It does not preserve order of elements.
    /// </summary>
    /// <typeparam name="E"></typeparam>
    public class Bag<E> {
        private E[] data;
        private int size = 0;

        /// <summary>
        /// Empty Bag with an initial capacity of 64.
        /// </summary>
        public Bag() : this(64) { }

        /// <summary>
        /// Empty Bag with the specified initial capacity.
        /// </summary>
        /// <param name="capacity"></param>
        public Bag(int capacity) {
            data = new E[capacity];
        }

        /// <summary>
        /// Removes the element at the specified position in this Bag. Order of elements is not preserved.
        /// </summary>
        /// <param name="index"></param>
        /// <returns>element that was removed from the Bag.</returns>
        public E Remove(int index) {
            E e = data[index]; // make copy of element to remove so it can be returned
            data[index] = data[--size]; // overwrite item to remove with last element
            data[size] = default(E); // null last element, so gc can do its work
            return e;
        }

        /// <summary>
        /// Removes and return the last object in the bag.
        /// </summary>
        /// <returns>the last object in the bag, null if empty.</returns>
        public E RemoveLast() {
            if(size > 0) {
                E e = data[--size];
                data[size] = default(E);
                return e;
            }

            return default(E);
        }

        /// <summary>
        /// Removes the first occurrence of the specified element from this Bag, if it is present. If the Bag does not contain the
        /// element, it is unchanged. It does not preserve order of elements.
        /// </summary>
        /// <param name="e"></param>
        /// <returns>true if the element was removed.</returns>
        public bool Remove(E e) {
            for(int i = 0; i < size; i++) {
                E e2 = data[i];

                if(e.Equals(e2)) {
                    data[i] = data[--size]; // overwrite item to remove with last element
                    data[size] = default(E); // null last element, so gc can do its work
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Check if bag contains this element. The operator == is used to check for equality.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool Contains(E e) {
            for(int i = 0; size > i; i++) {
                if(e.Equals(data[i])) {
                    return true;
                }
            }
            return false;
        }

        public E this[int index] {
            get { return data[index]; }
            set { data[index] = value; }
        }

        /// <summary>
        /// </summary>
        /// <param name="index"></param>
        /// <returns>the element at the specified position in Bag.</returns>
        public E Get(int index) {
            return data[index];
        }

        /// <summary>
        /// the number of elements in this bag.
        /// </summary>
        public int Count {
            get {
                return size;
            }
        }

        /// <summary>
        /// the number of elements the bag can hold without growing.
        /// </summary>
        public int Capacity {
            get {
                return data.Length;
            }
        }

        /// <summary>
        /// whether or not the index is within the bounds of the collection
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool IsIndexWithinBounds(int index) {
            return index < Capacity;
        }

        /// <summary>
        /// true if this list contains no elements
        /// </summary>
        public bool IsEmpty {
            get {
                return size == 0;
            }
        }

        /// <summary>
        /// Adds the specified element to the end of this bag. if needed also increases the capacity of the bag.
        /// </summary>
        /// <param name="e"></param>
        public void Add(E e) {
            // is size greater than capacity increase capacity
            if(size == data.Length) {
                Grow();
            }

            data[size++] = e;
        }

        /// <summary>
        /// Set element at specified index in the bag.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="e"></param>
        public void Set(int index, E e) {
            if(index >= data.Length) {
                Grow(index * 2);
            }
            size = index + 1;
            data[index] = e;
        }

        /// <summary>
        /// Removes all of the elements from this bag. The bag will be empty after this call returns.
        /// </summary>
        public void Clear() {
            // null all elements so gc can clean up
            for(int i = 0; i < size; i++) {
                data[i] = default(E);
            }

            size = 0;
        }

        private void Grow() {
            int newCapacity = (data.Length * 3) / 2 + 1;
            Grow(newCapacity);
        }

        private void Grow(int newCapacity) {
            E[] oldData = data;
            data = new E[newCapacity];
            oldData.CopyTo(data, 0);
        }
    }
}
